from typing import Iterable
from jinja2 import Environment, PackageLoader
from inflection import camelize, underscore

from robot_language.generator.config import Config
from robot_language.generator.generator import Generator
from robot_language.model.model import Model
from robot_language.model.skillset import SkillSet
from robot_language.model.skill import Skill
from robot_language.model.resource_expr import *
from .config import PDDLConfig


class PDDLGenerator(Generator):
    def __init__(self, config: PDDLConfig, model: Model) -> None:
        super().__init__(config, model, override=True)
        self.__env = Environment(loader=PackageLoader(
            'skills2pddl', 'templates'))
        self.__env.filters['camelize'] = camelize
        self.__env.filters['underscore'] = underscore
        self.__env.filters['used_resources'] = PDDLGenerator.used_resources
        self.__env.filters['resource_expression'] = PDDLGenerator.resource_expression

    @property
    def env(self) -> Environment:
        return self.__env

    @staticmethod
    def resource_expression(expr: ResourceExpr) -> str:
        if isinstance(expr, ResourceStateEqExpr):
            return f"(resource_{expr.resource.name} RESOURCE_{expr.resource.name.upper()}_STATE_{expr.state.upper()})"
        elif isinstance(expr, ResourceStateNeExpr):
            return f"(not (resource_{expr.resource.name} RESOURCE_{expr.resource.name.upper()}_STATE_{expr.state.upper()}))"
        elif isinstance(expr, BoolValueExpr):
            return expr.to_rl()
        elif isinstance(expr, ResourceNotExpr):
            return f"(not {PDDLGenerator.resource_expression(expr.expr)})"
        elif isinstance(expr, ResourceBinaryExpr):
            match expr.operator:
                case ResourceBinaryOperator.AND:
                    return f"(and {PDDLGenerator.resource_expression(expr.left)} {PDDLGenerator.resource_expression(expr.right)})"
                case ResourceBinaryOperator.OR:
                    raise NotImplementedError()
                case ResourceBinaryOperator.IMPLY:
                    raise NotImplementedError()

    @staticmethod
    def used_resources(skill: Skill) -> Iterable[str]:
        in_guard = [r.name for p in skill.preconditions for r in p.guard.used_resources()]
        in_start = [p.resource.name for p in skill.start]
        in_inv_guard = [r.name for p in skill.invariants for r in p.guard.used_resources()]
        in_inv_failure = [p.resource.name for s in skill.invariants for p in s.failure]
        in_int_effects = [p.resource.name for p in skill.interrupt.effects]
        in_int_post = [r.name for p in skill.interrupt.postcondition for r in p.used_resources()] if skill.interrupt.postcondition else []
        in_success_post = [r.name for m in skill.success if m.postcondition for p in m.postcondition for r in p.used_resources()]
        in_success_effects = [p.resource.name for m in skill.success for p in m.effects]
        in_failure_post = [r.name for m in skill.failure if m.postcondition for p in m.postcondition for r in p.used_resources()]
        in_failure_effects = [p.resource.name for m in skill.failure for p in m.effects]
        return set(in_guard + in_start + in_inv_failure + in_inv_guard + 
                   in_int_post + in_int_effects + in_success_effects + in_success_post +
                   in_failure_effects + in_failure_post)

    def create_domain(self, skillset: SkillSet):
        self.save_render("./", 'domain.pddl', skillset, file=f"{skillset.name}_domain.pddl")

    def create_problem(self, skillset: SkillSet):
        self.save_render("./", 'problem.pddl', skillset, file=f"{skillset.name}_problem.pddl")

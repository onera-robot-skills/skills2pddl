from typing import Dict, Iterable
from robot_language.generator.config import Config

class PDDLConfig(Config):
    def __init__(self, folder: str = None) -> None:
        super().__init__(folder)
        self.__modes : Dict[str, str] = dict()

    @property
    def skills(self) -> Iterable[str]:
        return self.__modes.keys()

    def get_mode(self, skill: str) -> str:
        return self.__modes[skill]

    def load_from_data(self, data):
        super().load_from_data(data)
        if data.get('mode'):
            for m in data['mode']:
                if m.get('skill') and m.get('mode'):
                    self.__modes[m.get('skill')] = m.get("mode")

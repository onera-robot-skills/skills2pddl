(define (domain {{ skillset.name | lower }})
    (:requirements :strips :typing)
    (:types 
{%- for t in model.types %}
        {{ t.name }}
{%- endfor %}
{%- for r in skillset.resources %}
        RESOURCE_{{r.name | upper }}_STATE
{%- endfor %}
    )
    (:constants 
{%- for r in skillset.resources %}
    {%- for s in r.states %}
        RESOURCE_{{r.name | upper }}_STATE_{{ s | upper }} - RESOURCE_{{r.name | upper }}_STATE
    {%- endfor %}
{%- endfor %}
    )
    (:predicates
{%- for r in skillset.resources %}
        (resource_{{ r.name }} ?state - RESOURCE_{{r.name | upper }}_STATE)
{%- endfor %}
    )
{% for sk in skillset.skills if sk.name in config.skills %}
    (:action {{sk.name | lower }}
        :parameters (
{%- for i in sk.input %}
            ?i_{{ i.name }} - {{ i.type.name }}
{%- endfor %}
        )
        :precondition (and
{%- for p in sk.preconditions %}
            ; precond {{ p.name }}
            {{ p.guard | resource_expression }}
{%- endfor %}
        )
        :effect (and
            ; start effect
{%- for p in sk.start %}
            ; (resource_{{ p.resource.name }} RESOURCE_{{ p.resource.name | upper }}_STATE_{{ p.state | upper }})
{%- endfor %}
            ; mode '{{ config.get_mode(sk.name) }}' effect
{%- if config.get_mode(sk.name) == "interrupt" %}
    {%- for e in sk.interrupt.effects %}
            (resource_{{ e.resource.name }} RESOURCE_{{ e.resource.name | upper }}_STATE_{{ e.state | upper }})
    {%- endfor %}
    {%- if sk.interrupt.postcondition %}
            {{ sk.interrupt.postcondition | resource_expression }}
    {%- endif %}
{%- endif %}
{%- for m in sk.success if m.name == config.get_mode(sk.name) %}
    {%- for e in m.effects %}
            (resource_{{ e.resource.name }} RESOURCE_{{ e.resource.name | upper }}_STATE_{{ e.state | upper }})
    {%- endfor %}
    {%- if m.postcondition %}
            {{ m.postcondition | resource_expression }}
    {%- endif %}
{%- endfor %}
            ; output
{%- for v in sk.output %}
            ; ({{ v.name }} - {{ v.type }})
{%- endfor %}
        )
    )
{% endfor %}
)

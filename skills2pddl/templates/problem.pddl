(define (problem {{ skillset.name | lower }}_problem)
    (:domain {{ skillset.name | lower }})

    (:objects
    )

    (:init
        ; resource initial states
{%- for r in skillset.resources %}
        (resource_{{ r.name | lower }} RESOURCE_{{r.name | upper }}_STATE_{{ r.initial | upper }})
{%- endfor %}

    )

    (:goal
        (and
        )
    )

)

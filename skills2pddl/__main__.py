import argparse

from robot_language.tool import tool
from robot_language.parsing import parse_file, RobotLangException
from robot_language.generator import Config

from .pddl import PDDLGenerator
from .config import PDDLConfig

def main():
    try:
        parser = argparse.ArgumentParser(
            prog='python -m skills2pddl',
            description='Translator from robot skills to PDDL')
        parser.add_argument('file', type=str, help='model file')
        parser.add_argument('-s', '--skillset', type=str, help='specify skillset in case the model contains multiple')
        parser.add_argument('-o', '--output-file', required=False,
                            type=str, help='output file')
        parser.add_argument('-v', '--version', required=False, default="1.2",
                            choices=["1.2"], type=str, help='PDDL version')
        parser.add_argument('-c', '--config', required=False,
                            type=str, help='Configuration file')
        parser.add_argument('--verbose', required=False,
                            type=int, help='set verbosity (ERROR=0, WARNING=1, VERIF=2, INFO=3, DEBUG=4)')
        args = parser.parse_args()

        if args.verbose:
            tool.set_verbosity(args.verbose)

        model = {}
        # Parsing
        tool.start_phase('Parsing')
        tree, model = parse_file(args.file)
        if not tool.is_ok:
            return
        # Resolve
        tool.start_phase('Resolve')
        model.resolve()
        if not tool.is_ok:
            return
        # Check
        tool.start_phase('Check')
        model.check()
        if not tool.is_ok:
            return
        # Config
        config = PDDLConfig()
        tool.start_phase('Config')
        config.load_from_json_file(args.config)
        if not tool.is_ok:
            return
        # PDDL
        if args.skillset:
            sk = model.get_skillset(args.skillset)
        else:
            sk = model.skillsets[0]
        tool.start_phase(f'Generate PDDL for {sk.name}')
        generator = PDDLGenerator(config, model)
        generator.create_domain(sk)
        generator.create_problem(sk)
        if not tool.is_ok:
            return

    except RobotLangException as e:
        print(e)


if __name__ == '__main__':
    main()

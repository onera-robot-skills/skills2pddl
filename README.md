# Install

1. Install robot language

```bash
git clone https://gitlab.com/p2142/robot_language
pip3 install --user robot_language
```

2. Install the GUI generator

```bash
git clone git@gitlab.com:onera-robot-skills/skillset_gui_generator.git
pip3 install --user skillset_gui_generator
```

# Usage

```bash
python3 -m skillset_gui_generator model.rl -g file.json -d src/
```
    
It creates a `<skillset>_skillset_gui_widgets` package that depends on the skillset interfaces package and the `gui_tools` package.


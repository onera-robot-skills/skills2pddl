#!/usr/bin/env python3

from distutils.core import setup


setup(name='skills2pddl',
    version='0.1',
    description='Translation from Skills models to PDDL domains',
    author='Charles Lesire',
    author_email='charles.lesire@onera.fr',
    packages=[
        'skills2pddl'],
    package_dir={
        'skills2pddl': 'skills2pddl'
    },
    package_data={
        'skills2pddl': ['templates/*']
    }
)
